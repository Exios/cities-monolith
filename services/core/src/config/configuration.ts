import { join } from 'path';
import { parseBoolean } from 'src/utils/parsers/parseBoolean';
import { parseNumber } from 'src/utils/parsers/parseNumber';
import { parseString } from 'src/utils/parsers/parseString';

export const configuration = () => ({
  env: parseString(process.env.NODE_ENV, 'develop'),
  isLocal: parseBoolean(process.env.IS_LOCAL, false),
  port: parseNumber(process.env.PORT, 3000),
  apiPrefix: parseString(process.env.API_PREFIX, 'api'),
  documentationPrefix: parseString(process.env.DOCUMENTATION_PREFIX, 'apidoc'),
  database: {
    type: parseString(process.env.DATABASE_TYPE, 'postgres'),
    host: parseString(process.env.DATABASE_HOST, 'postgres'),
    port: parseNumber(process.env.DATABASE_PORT, 5432),
    username: parseString(process.env.DATABASE_USERNAME, ''),
    password: parseString(process.env.DATABASE_PASSWORD, ''),
    database: parseString(process.env.DATABASE_NAME, 'postgres'),
    entities: [join(__dirname, '../db/entities', '**', '*.entity.{ts,js}')],
    migrations: [
      join(__dirname, '../db/migrations', '**', '*.{ts,js}'),
      join(__dirname, '../db', '**', '*seed.{ts,js}'),
    ],
    migrationsRun: parseBoolean(process.env.DATABASE_RUN_MIGRATIONS, false),
    synchronize: true,
    keepConnectionAlive: true,
    cli: {
      migrationsDir: '../db/migrations',
    },
  },
  documentation: {
    title: parseString(process.env.DOCUMENATION_TITLE, 'Monolith'),
    description: parseString(
      process.env.DOCUMENATION_DESCRIPTION,
      'The Monolith core api',
    ),
    version: parseString(process.env.DOCUMENATION_VERSION, '0.0.1'),
    tag: parseString(process.env.DOCUMENATION_TAG, 'monolith'),
  },
  jwt: {
    secret: parseString(process.env.JWT_SECRET, 'secret'),
    expiresIn: parseString(process.env.JWT_EXPIRE, '360s'),
  },
});
