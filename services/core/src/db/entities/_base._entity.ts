import {
  BaseEntity as TypeormBaseEntity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ClassConstructor, plainToClass } from 'class-transformer';
import { BaseDto } from '../dtos/_base._dto';

export class Base extends TypeormBaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: number;

  @CreateDateColumn({ precision: 3 })
  createdAt: Date;

  @UpdateDateColumn({ precision: 3 })
  updatedAt: Date;

  toDto<T extends BaseDto>(
    cls: ClassConstructor<T>,
    extraProps?: Partial<ClassConstructor<T>>,
  ): T {
    return plainToClass<T, this>(
      cls,
      { ...this, ...extraProps },
      { excludeExtraneousValues: true, enableImplicitConversion: true },
    );
  }
}
