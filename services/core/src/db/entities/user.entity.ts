import { Column, Entity, ManyToOne } from 'typeorm';
import { City } from './city.entity';
import { Base } from './_base._entity';

@Entity()
export class User extends Base {
  @Column({ unique: true })
  email: string;

  @Column()
  password: string;

  @Column()
  name: string;

  @ManyToOne(() => City, (city) => city.users, {
    onDelete: 'RESTRICT',
    nullable: true,
  })
  city: City;

  @Column({ nullable: true, precision: 3 })
  birthday: Date;
}
