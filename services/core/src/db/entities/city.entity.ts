import { Column, Entity, OneToMany } from 'typeorm';
import { User } from './user.entity';
import { Base } from './_base._entity';

@Entity()
export class City extends Base {
  @Column({ unique: true })
  name: string;

  @OneToMany(() => User, (user) => user.city, { nullable: true })
  users: User[];
}
