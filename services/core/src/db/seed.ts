import { CityNames } from 'src/const/city/enums/names.enum';
import { MigrationInterface, QueryRunner } from 'typeorm';
import { City as CityEntity } from './entities/city.entity';

export class Seed1676749161153 implements MigrationInterface {
  private async createCities(queryRunner: QueryRunner) {
    const cityNames = Object.values(CityNames);

    const promises = cityNames.map((cityName) =>
      queryRunner.manager.save(
        queryRunner.manager.create<CityEntity>(CityEntity, {
          name: cityName,
        }),
      ),
    );

    await Promise.all(promises);
  }

  public async up(queryRunner: QueryRunner): Promise<void> {
    await this.createCities(queryRunner);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
