import { Expose } from 'class-transformer';
import { CityDto } from './city.dto';
import { BaseDto } from './_base._dto';

export class UserDto extends BaseDto {
  @Expose()
  email: string;

  @Expose()
  name: string;

  @Expose()
  city: CityDto;

  @Expose()
  birthday: Date;
}
