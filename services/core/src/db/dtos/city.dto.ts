import { Expose } from 'class-transformer';
import { BaseDto } from './_base._dto';

export class CityDto extends BaseDto {
  @Expose()
  name: string;
}
