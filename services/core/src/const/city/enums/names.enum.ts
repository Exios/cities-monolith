export enum CityNames {
  SaintPetersburg = 'saintPetersburg',
  Moscow = 'moscow',
  Minsk = 'minsk',
  Vladivostok = 'vladivostok',
}
