export enum UserNameProps {
  MinLength = 3,
  MaxLength = 10,
}
