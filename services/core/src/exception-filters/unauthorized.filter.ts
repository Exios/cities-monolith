import { UnauthorizedException } from '@nestjs/common';
import { ExceptionFilter, Catch, ArgumentsHost } from '@nestjs/common';

@Catch(UnauthorizedException)
export class UnauthorizedExceptionFilter
  implements ExceptionFilter<UnauthorizedException>
{
  catch(exception: UnauthorizedException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const ctxResponse = ctx.getResponse();

    const statusCode = exception.getStatus();
    const response = exception.getResponse();

    const body = {
      statusCode,
      errors: {
        code: 'common__unauthorized',
        message: response['message'],
        errorJsonObject: response['error'],
      },
    };

    ctxResponse.status(statusCode).json(body);
  }
}
