import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  HttpStatus,
  Logger,
} from '@nestjs/common';

export interface IHttpError {
  statusCode: number;
  errors: {
    message: string;
    code: string;
  }[];
}

@Catch()
export class AnyExceptionsFilter implements ExceptionFilter {
  private readonly logger = new Logger();

  catch(exception: unknown, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const ctxResponse = ctx.getResponse();
    const ctxRequest = ctx.getRequest();
    const isHttpException: boolean = exception instanceof HttpException;

    const body: IHttpError = {
      statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
      errors: [{ code: 'common__internal', message: 'Internal server error' }],
    };

    if (isHttpException) {
      const httpException = exception as HttpException;

      const statusCode = httpException.getStatus();
      const response = httpException.getResponse();

      body.statusCode = statusCode;
      body.errors = response['message'];
    }

    const errorLoggerMessage = `Error on request: [${ctxRequest.method}] ${ctxRequest.path} | Response status: ${body.statusCode}`;
    if (!isHttpException) {
      const defaultException = exception as Error;

      this.logger.error(errorLoggerMessage, defaultException.stack);
    } else {
      this.logger.error(errorLoggerMessage);
    }

    ctxResponse.status(body.statusCode).json(body);
  }
}
