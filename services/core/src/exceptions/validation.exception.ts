import { BadRequestException } from '@nestjs/common';
import { ValidationError } from 'class-validator';

export const exceptionRequest = (validationErrors: ValidationError[] = []) => {
  const mapper = mapErrors(validationErrors);
  const error = new BadRequestException(mapper);

  return error;
};

const mapErrors = (validationErrors: ValidationError[]) => {
  const formatted = validationErrors
    .map(({ property, constraints }: ValidationError) => {
      const constraintsArray = Object.entries(constraints);

      return constraintsArray.map(([code, message]) => ({
        code: `${property}__${code}`,
        message,
      }));
    })
    .flat();

  return formatted;
};
