import { ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { useContainer } from 'class-validator';
import * as cookieParser from 'cookie-parser';
import { AppModule } from './app.module';
import { CitiesModule } from './cities/cities.module';
import { AnyExceptionsFilter } from './exception-filters/any.filter';
import { UnauthorizedExceptionFilter } from './exception-filters/unauthorized.filter';
import { exceptionRequest } from './exceptions/validation.exception';
import { LoggerInterceptor } from './interceptors/logger.interceptor';
import { TransformInterceptor } from './interceptors/transform.interceptor';
import { UsersModule } from './users/users.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });
  useContainer(app.select(UsersModule), { fallbackOnErrors: true });
  useContainer(app.select(CitiesModule), { fallbackOnErrors: true });

  const config = app.get(ConfigService);

  const apiPrefix = config.get('apiPrefix');
  const documentationPrefix = config.get('documentationPrefix');
  const appPort = config.get('port');
  const documentation = config.get('documentation');

  app.setGlobalPrefix(apiPrefix);

  const documentationOptions = new DocumentBuilder()
    .setTitle(documentation.title)
    .setDescription(documentation.description)
    .setVersion(documentation.version)
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, documentationOptions);
  SwaggerModule.setup(documentationPrefix, app, document);

  app.useGlobalPipes(
    new ValidationPipe({
      forbidNonWhitelisted: true,
      forbidUnknownValues: true,
      skipUndefinedProperties: true,
      whitelist: true,
      exceptionFactory: exceptionRequest,
      // exceptionFactory: exceptionFactory
    }),
  );
  app.useGlobalFilters(new AnyExceptionsFilter());
  app.useGlobalFilters(new UnauthorizedExceptionFilter());
  app.useGlobalInterceptors(
    new LoggerInterceptor(),
    new TransformInterceptor(),
  );

  app.use(cookieParser());

  await app.listen(appPort);
}
bootstrap();
