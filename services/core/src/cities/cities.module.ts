import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CitiesService } from './cities.service';
import { CitiesController } from './cities.controller';
import { City as CityEntity } from '../db/entities/city.entity';
import { CityShouldExistsConstraint } from './validators/city-should-exists.validator';

@Module({
  imports: [TypeOrmModule.forFeature([CityEntity])],
  exports: [CitiesService, TypeOrmModule],
  controllers: [CitiesController],
  providers: [CitiesService, CityShouldExistsConstraint],
})
export class CitiesModule {}
