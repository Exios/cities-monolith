export interface IFindCitiesDto {
  limit: number;
  offset: number;
  excludeIds?: number[];
}
