import { Controller, Get, UseGuards, Query } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/guards/jwt.guard';
import { CityDto } from 'src/db/dtos/city.dto';
import { parseNumber } from 'src/utils/parsers/parseNumber';
import { CitiesService } from './cities.service';
import { FindCitiesRequest } from './requests/find-cities.request';

@ApiTags('cities')
@Controller('cities')
export class CitiesController {
  constructor(private readonly citiesService: CitiesService) {}

  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @Get()
  async find(@Query() query: FindCitiesRequest) {
    const { limit, offset } = query;

    const cities = await this.citiesService.find({
      limit: parseNumber(limit, 10),
      offset: parseNumber(offset, 0),
    });

    return { cities: cities.map((c) => c.toDto(CityDto)) };
  }
}
