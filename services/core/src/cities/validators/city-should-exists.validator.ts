import { Injectable } from '@nestjs/common';
import {
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { CitiesService } from '../cities.service';

@ValidatorConstraint({ name: 'shouldExists', async: true })
@Injectable()
export class CityShouldExistsConstraint
  implements ValidatorConstraintInterface
{
  constructor(protected readonly citiesService: CitiesService) {}

  async validate(text: string) {
    const city = await this.citiesService.findById({
      cityId: text,
    });

    return Boolean(city);
  }
}

export function CityShouldExists(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: [],
      validator: CityShouldExistsConstraint,
    });
  };
}
