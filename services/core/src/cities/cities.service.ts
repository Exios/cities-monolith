import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Not, Repository } from 'typeorm';
import { City as CityEntity } from '../db/entities/city.entity';
import { IFindCitiesDto } from './dtos/find-cities.dto';
import { IFindCityByIdDto } from './dtos/find-city-by-id.dto';

@Injectable()
export class CitiesService {
  constructor(
    @InjectRepository(CityEntity)
    private citiesRepository: Repository<CityEntity>,
  ) {}

  async find(data: IFindCitiesDto) {
    const { limit, offset, excludeIds = [] } = data;

    const users = await this.citiesRepository.find({
      where: {
        id: Not(In(excludeIds)),
      },
      take: limit,
      skip: offset,
    });

    return users;
  }

  async findById(data: IFindCityByIdDto) {
    const { cityId } = data;

    const city = await this.citiesRepository.findOne({
      where: { id: cityId as unknown as number },
    });

    return city;
  }
}
