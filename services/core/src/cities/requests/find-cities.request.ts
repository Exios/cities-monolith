import { IsNumber, Min, Max, IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';

export interface IFindCitiesRequest {
  limit?: string;
  offset?: string;
}

export class FindCitiesRequest implements IFindCitiesRequest {
  @IsOptional()
  @IsNumber({}, { message: 'Name should be number' })
  @Min(1)
  @Max(10)
  @Transform(({ value }) => Number(value))
  @ApiProperty({ description: 'Returned entities limit', required: false })
  readonly limit?: string;

  @IsOptional()
  @IsNumber({}, { message: 'Name should be number' })
  @Min(0)
  @Transform(({ value }) => Number(value))
  @ApiProperty({ description: 'Count of skipped entities', required: false })
  readonly offset?: string;
}
