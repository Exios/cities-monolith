import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Response<T> {
  result: T;
}

export const TRANSFORM_INTERCEPTOR_KEY = 'transform';

@Injectable()
export class TransformInterceptor<T>
  implements NestInterceptor<T, Response<T>>
{
  intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Observable<Response<T>> {
    return next
      .handle()
      .pipe(
        map((result) =>
          context.getHandler()[TRANSFORM_INTERCEPTOR_KEY]
            ? result
            : { result: result || '' },
        ),
      );
  }
}
