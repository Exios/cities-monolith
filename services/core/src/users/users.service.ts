import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Not, In, Repository } from 'typeorm';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';
import * as bcrypt from 'bcrypt';
import { User as UserEntity } from '../db/entities/user.entity';
import { ICreateUserDto } from './dtos/create-user.dto';
import { IFindUsersDto } from './dtos/find-users.dto';
import { IFindUserByIdDto } from './dtos/find-user-by-id.dto';
import { IFindUserByEmailDto } from './dtos/find-user-by-email.dto';
import { IUpdateUserDto } from './dtos/update-user.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserEntity)
    private usersRepository: Repository<UserEntity>,
  ) {}

  async create(data: ICreateUserDto) {
    const { name, email, password } = data;

    const passwordHash = bcrypt.hashSync(password, 10);

    const user = await this.usersRepository
      .create({
        name,
        email,
        password: passwordHash,
      })
      .save();

    return user;
  }

  async find(data: IFindUsersDto) {
    const { limit, offset, excludeIds = [] } = data;

    const users = await this.usersRepository.find({
      where: {
        id: Not(In(excludeIds)),
      },
      take: limit,
      skip: offset,
      relations: { city: true },
    });

    return users;
  }

  async findById(data: IFindUserByIdDto) {
    const { userId } = data;

    const user = await this.usersRepository.findOne({
      where: { id: userId },
      relations: { city: true },
    });

    return user;
  }

  async findByEmail(data: IFindUserByEmailDto) {
    const { email } = data;

    const user = await this.usersRepository.findOne({
      where: { email },
      relations: { city: true },
    });

    return user;
  }

  async update(data: IUpdateUserDto) {
    const { name, password, cityId, birthday, userId } = data;

    const updateQuery: QueryDeepPartialEntity<UserEntity> = {
      birthday: new Date(birthday),
    };

    if (data.name) {
      updateQuery.name = name;
    }
    if (data.password) {
      const passwordHash = bcrypt.hashSync(password, 10);

      updateQuery.password = passwordHash;
    }
    if (data.cityId) {
      updateQuery.city = { id: cityId };
    }

    await this.usersRepository.update({ id: userId }, updateQuery);

    const user = await this.findById({ userId });

    return user;
  }
}
