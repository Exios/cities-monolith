import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Query,
  UseGuards,
  Request,
} from '@nestjs/common';
import { parseNumber } from 'src/utils/parsers/parseNumber';
import { UserDto } from 'src/db/dtos/user.dto';
import { UsersService } from './users.service';
import { UpdateUserRequest } from './requests/update-user.request';
import { FindUsersRequest } from './requests/find-users.request';
import { CreateUserRequest } from './requests/create-user.request';
import { JwtAuthGuard } from 'src/auth/guards/jwt.guard';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

@ApiTags('users')
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  async create(@Body() body: CreateUserRequest) {
    const { name, email, password } = body;

    const user = await this.usersService.create({ name, email, password });

    return { user: user.toDto(UserDto) };
  }

  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @Get()
  async find(@Query() query: FindUsersRequest) {
    const { limit, offset } = query;

    const users = await this.usersService.find({
      limit: parseNumber(limit, 10),
      offset: parseNumber(offset, 0),
    });

    return { users: users.map((u) => u.toDto(UserDto)) };
  }

  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @Get('me')
  async findMe(@Request() req) {
    const { userId } = req.user;

    const user = await this.usersService.findById({ userId });

    return { user: user.toDto(UserDto) };
  }

  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @Patch('me')
  async update(@Request() req, @Body() body: UpdateUserRequest) {
    const { id: userId } = req.user;
    const { name, password, cityId, birthday } = body;

    const user = await this.usersService.update({
      name,
      password,
      cityId,
      birthday,
      userId,
    });

    return { user: user.toDto(UserDto) };
  }
}
