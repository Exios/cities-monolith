import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { User as UserEntity } from '../db/entities/user.entity';
import { UserEmailShouldBeUniqueConstraint } from './validators/email-should-be-unique.validator';

@Module({
  imports: [TypeOrmModule.forFeature([UserEntity])],
  exports: [UsersService, TypeOrmModule],
  controllers: [UsersController],
  providers: [UsersService, UserEmailShouldBeUniqueConstraint],
})
export class UsersModule {}
