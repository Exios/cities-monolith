export interface IUpdateUserDto {
  name?: string;
  password?: string;
  cityId?: number;
  birthday: Date;
  userId: number;
}
