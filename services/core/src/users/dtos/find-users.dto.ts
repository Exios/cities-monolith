export interface IFindUsersDto {
  limit: number;
  offset: number;
  excludeIds?: number[];
}
