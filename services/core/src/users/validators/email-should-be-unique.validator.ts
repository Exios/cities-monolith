import { Injectable } from '@nestjs/common';
import {
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { UsersService } from '../users.service';

@ValidatorConstraint({ name: 'shouldBeUnique', async: true })
@Injectable()
export class UserEmailShouldBeUniqueConstraint
  implements ValidatorConstraintInterface
{
  constructor(protected readonly usersService: UsersService) {}

  async validate(text: string) {
    const user = await this.usersService.findByEmail({
      email: text,
    });

    return !user;
  }
}

export function UserEmailShouldBeUnique(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: [],
      validator: UserEmailShouldBeUniqueConstraint,
    });
  };
}
