import {
  IsDateString,
  IsDefined,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { UserNameProps } from 'src/const/user/props/name.props';
import { UserPasswordProps } from 'src/const/user/props/password.props';
import { CityShouldExists } from 'src/cities/validators/city-should-exists.validator';

export interface IUpdateUserRequest {
  name?: string;
  password?: string;
  cityId?: number;
  birthday: Date;
}

export class UpdateUserRequest implements IUpdateUserRequest {
  @IsOptional()
  @IsString({ message: 'Name should be string' })
  @MinLength(UserNameProps.MinLength, {
    message: `Min name length is ${UserNameProps.MinLength}`,
  })
  @MaxLength(UserNameProps.MaxLength, {
    message: `Max name length is ${UserNameProps.MaxLength}'`,
  })
  @ApiProperty({ description: 'User name', required: false })
  readonly name?: string;

  @IsOptional()
  @IsString({ message: 'Password should be string' })
  @MinLength(UserPasswordProps.MinLength, {
    message: `Min password length is ${UserPasswordProps.MinLength}`,
  })
  @MaxLength(UserPasswordProps.MaxLength, {
    message: `Max password length is ${UserPasswordProps.MaxLength}`,
  })
  @ApiProperty({ description: 'User password', required: false })
  readonly password?: string;

  @IsOptional()
  @IsString({ message: 'Password should be string' })
  @CityShouldExists({ message: 'City should exists' })
  @ApiProperty({ description: 'User city id', required: false })
  readonly cityId?: number;

  @IsDefined({ message: 'Birthday date not defined' })
  @IsDateString({ strict: true }, { message: 'Birthday should be date' })
  @ApiProperty({ description: 'User birthday date', required: true })
  readonly birthday: Date;
}
