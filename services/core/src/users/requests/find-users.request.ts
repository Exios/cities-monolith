import { IsNumber, Min, Max, IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';

export interface IFindUsersRequest {
  limit?: string;
  offset?: string;
}

export class FindUsersRequest implements IFindUsersRequest {
  @IsOptional()
  @IsNumber({}, { message: 'Name should be number' })
  @Min(1)
  @Max(10)
  @Transform(({ value }) => Number(value))
  @ApiProperty({ description: 'Returned entities limit', required: false })
  readonly limit?: string;

  @IsOptional()
  @IsNumber({}, { message: 'Name should be number' })
  @Min(0)
  @Transform(({ value }) => Number(value))
  @ApiProperty({ description: 'Count of skipped entities', required: false })
  readonly offset?: string;
}
