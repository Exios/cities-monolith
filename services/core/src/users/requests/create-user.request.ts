import {
  IsDefined,
  IsEmail,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { UserEmailShouldBeUnique } from '../validators/email-should-be-unique.validator';
import { UserNameProps } from 'src/const/user/props/name.props';
import { UserPasswordProps } from 'src/const/user/props/password.props';

export interface ICreateUserRequest {
  name: string;
  email: string;
  password: string;
}

export class CreateUserRequest implements ICreateUserRequest {
  @IsDefined({ message: 'Name not defined' })
  @IsString({ message: 'Name should be string' })
  @MinLength(UserNameProps.MinLength, {
    message: `Min password length is ${UserNameProps.MinLength}`,
  })
  @MaxLength(UserNameProps.MaxLength, {
    message: `Max password length is ${UserNameProps.MaxLength}`,
  })
  @ApiProperty({ description: 'User name', required: true })
  readonly name: string;

  @IsDefined({ message: 'Email not defined' })
  @IsEmail({}, { message: 'Email is invalid' })
  @UserEmailShouldBeUnique({ message: 'Email duplicate' })
  @ApiProperty({ description: 'User email', required: true })
  readonly email: string;

  @IsDefined({ message: 'Password not defined' })
  @IsString({ message: 'Password should be string' })
  @MinLength(UserPasswordProps.MinLength, {
    message: `Min password length is ${UserPasswordProps.MinLength}`,
  })
  @MaxLength(UserPasswordProps.MaxLength, {
    message: `Max password length is ${UserPasswordProps.MaxLength}`,
  })
  @ApiProperty({ description: 'User password', required: true })
  readonly password: string;
}
