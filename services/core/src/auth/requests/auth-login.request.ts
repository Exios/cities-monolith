import {
  IsDefined,
  IsEmail,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export interface IAuthLoginRequest {
  email: string;
  password: string;
}

export class AuthLoginRequest implements IAuthLoginRequest {
  @IsDefined({ message: 'Email not defined' })
  @IsEmail({}, { message: 'Email is invalid' })
  @ApiProperty({ description: 'User email', required: true })
  readonly email: string;

  @IsDefined({ message: 'Password not defined' })
  @IsString({ message: 'Password should be string' })
  @MinLength(8, { message: 'Min password length is 8' })
  @MaxLength(15, { message: 'Max password length is 15' })
  @ApiProperty({ description: 'User password', required: true })
  readonly password: string;
}
