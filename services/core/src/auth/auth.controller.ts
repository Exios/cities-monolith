import { Controller, Request, Post, UseGuards, Body } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { LocalAuthGuard } from './guards/local.guard';
import { AuthService } from './auth.service';
import { AuthLoginRequest } from './requests/auth-login.request';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(@Request() req, @Body() body: AuthLoginRequest) {
    const { id: userId } = req.user;
    const { email } = body;

    const { accessToken } = await this.authService.login({
      email,
      userId,
    });

    return { accessToken };
  }
}
